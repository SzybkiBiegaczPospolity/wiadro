<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wędkujemy</title>
    <link rel="stylesheet" href="styl_2.css?v=1" type="text/css">
    <?php
    $polaczenie = mysqli_connect("localhost","root","","wedkowanie");
    ?>
</head>
<body>

<div class="naglowek">
    <h1>Portal dla wędkarzy</h1>
    </div>
<div class="blok_lewy">
    <h2>Ryby drapieżne naszych wód</h2>
    <ul>
        <?php
        $sql = "SELECT `nazwa` , `wystepowanie` FROM `ryby` WHERE `styl_zycia` = '1'";

        $result = mysqli_query($polaczenie,$sql);
        while($row = mysqli_fetch_array($result))
        {
            echo "<li>";
            echo $row['nazwa'];
            echo ", wystepowanie: ";
            echo $row['wystepowanie'];
            echo "</li>";
        }
        ?>
    </ul>
</div>
<div class="blok_prawy">
    <img src="ryba3.jpg?v=1" alt="">
    <a href="kwerendy.txt">Pobietrz kwerendy</a>
</div>
<div class="stopka">
    <p>Stronę wykonał: Maciej Pietrzykowski</p>
</div>
    
</body>
</html>