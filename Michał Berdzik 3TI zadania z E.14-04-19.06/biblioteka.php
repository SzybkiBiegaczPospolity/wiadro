<?php 
    $db = mysqli_connect("localhost","root","","biblioteka");
?>
<!DOCTYPE html>
<html lang="pl">

<head>
    <title>Biblioteka publiczna</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <header>
        <h2>Miejska Biblioteka Publiczna w Książkowicach</h2>
    </header>
    <aside id="lewy_blok">
        <h2>Dodaj czytelnika</h2>
        <form method="POST">
            <label>imię: <input type="text" name="imie"></label><br>
            <label>nazwisko: <input type="text" name="nazwisko"></label><br>
            <label>rok urodzenia: <input type="text" name="rok_urodzenia"></label><br>
            <input type="submit" value="DODAJ!">
        </form>
        <?php
            if(isset($_POST["imie"],$_POST["nazwisko"],$_POST["rok_urodzenia"])){
                $imie = $_POST["imie"];
                $nazwisko = $_POST["nazwisko"];
                $rok_urodzenia = $_POST["rok_urodzenia"];
                echo "Czytelnik: ".$nazwisko." został dodany do bazy danych";
                $kod1 = strtolower(substr($imie,0,2));
                $kod2 = substr($rok_urodzenia,2,2);
                $kod3 = strtolower(substr($nazwisko,0,2));
                $kod = $kod1.$kod2.$kod3;

                $sql1 =  "INSERT INTO `czytelnicy` VALUES(NULL, '$imie', '$nazwisko', '$kod')";
                $query1 = mysqli_query($db,$sql1);
            }
        ?>
    </aside>
    <main>
        <img src="biblioteka.png" alt="biblioteka">
        <h4>ul.Czytelnicza 25<br> 12-120 Książkowice<br> tel.: 123123123<br> e-mail: <a href="mailto: biuro@bib.pl">biuro@bib.pl</a></h4>
    </main>
    <aside id="prawy_blok">
        <h3>Nasi czytelnicy</h3>
        <?php
        $sql2 = "SELECT czytelnicy.imie,czytelnicy.nazwisko FROM `czytelnicy`;";
        $query2 = mysqli_query($db,$sql2);
        echo "<ul>";
        while($lista=mysqli_fetch_array($query2)){
                $dane = $lista["imie"]." ".$lista["nazwisko"]."<br>";
                echo "<li>".$dane."</li>";
        }
        echo "<ul>";
        mysqli_close($db);
        ?>
    </aside>
    <div id="eraser"></div>
    <footer>
        <p>Projekt strony: Michał Berdzik</p>
    </footer>
</body>

</html>